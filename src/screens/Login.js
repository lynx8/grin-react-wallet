import React, {Component} from 'react';
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from 'react-responsive-modal';

import {downloadFile} from '../lib/helpers/files';

import RedDot from '../components/RedDot';
/**
 * @name - 
 * @summary - 
 * @description - 
 * @prop - 
 */
export default class  extends Component {
/**
 * @name constructor
 * @summary - 
 * @description - 
 */
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      passwordErrors: [],
      seedPhrase: null,
      seedPhraseModal: false,
      intentions: {}
    }
  }

  onChangeText = (field) => (event) => {
    if(field && event) {
      this.setState({[field]: event.target.value});
    }
  }

  validatePassword = () => {
    let valid = true;

    if(this.state.password.length < 20) {
      toast(
        "Password must be longer that 20 characters. \n Almost as long as a Magician's spell 🤔",
        {className: 'warning-toast'}
      );
      valid = false;
    }

    if(this.state.password === "password") {
      toast(
        "Password must be longer that 20 characters. \n Almost as long as a Magician's spell 🤔",
        {className: 'warning-toast'}
      );
      valid = false;
    }

    if(this.state.password === "password") {
      toast(
        "Password must be longer that 20 characters. \n Almost as long as a Magician's spell 🤔",
        {className: 'warning-toast'}
      );
      valid = false;
    }

    return valid;
  }

  onSubmitPassword = () => {
    // TODO : password validation
    console.log('submit password');
    const isValid = this.validatePassword();
    if(isValid) {
      const seed = this.generateSeedPhrase();
      if(seed && seed.length === 12) {
        // saveSeedPhraseModal(seed);
      }
      this.onSeedPhraseModalOpen();
    }
  }

  generateSeedPhrase = () => {
    /*
    return this.state.password ?
      lib to generate random words
      : null
      this.setState({seedPhrase})
    */
   // does grin implementation have an API? https://github.com/mimblewimble/grin/blob/1b6573850420688f443334ae6949d9a8b515233f/keychain/src/mnemonic.rs
  }

  onSeedPhraseModalOpen = () => {
    this.setState({seedPhraseModal: true});
  };

  onSeedPhraseModalClose = () => {
    this.setState({seedPhraseModal: false});
  }

  downloadSeedPhrase = () => {
    downloadFile(this.state.seedPhrase, "Schminii_Grinii_Secret_Spell", ".txt")
  }


  saveSeedPhraseModal = (seedPhrase = []) => {
    // Metamask saves seedphrase to local file system - varies by machine, growser and version
    // ~/Library/Application\ Support/Google/Chrome/Default
    return (
      <div>
        <h3>Make sure you don't lose this</h3>
        {seedPhrase.map((word) => 
          <span className="seedphrase-item"> {word} </span>)}
      </div>
    )
  };

  checkIntentions = () => {
    const {intentions} = this.state;
    const intents = Object.keys(this.state.intentions);
    const totalIntentions = 2;
    const isIntentful = intents.length === totalIntentions
      ? intents.reduce((isIntentional, intent) => 
          isIntentional && intentions[intent], true)
      : false;

    if(isIntentful) {
      // this.downloadSeedPhrase
      toast("Seedphrase file downloaded");
    } else {
      toast("You must swear to all intentions");
    }
  }

  handleIntentions = (intent) => {
    const pastIntentions = this.state.intentions;
    const currentIntent = pastIntentions[intent] ? !pastIntentions[intent] : true;
    this.setState({intentions: {...pastIntentions, [intent]: currentIntent}})
  }

  render() {
    return (
      <div id="register-page-container" className="page">
        
        <ToastContainer />

        <div id="register-content-left">
          {/* marketing and educational copy + art */}
          <p> GRIN IS THE BEST. GET SCHMINNI GRINNI FOR THE MONII </p>
        </div>
      
        <div className="vertical-line"></div>

        <div id="register-content-right">
          <center id="register-input-container"> 
            <p> 
              Schminii Grinii wallet does not require any personal information.
              <br/>
              <br/>
              Input a <strong>strong</strong> password and an optional emoji username
            </p>

            <input
              label="Username"
              type="text"
              placeholder="Username - emojis only 🙃"
              value={this.state.username}
              onChange={this.onChangeText("username")}
              className="register-input"
            />
            
            <br/>

            <input
              label="Password"
              type="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.onChangeText("password")}
              className="register-input"
            />
            
            <RedDot id="register-red-button" radius="10vw">
              <button
                onClick={this.onSubmitPassword}
                className="red-dot-button"
              > 
                Cast Alchemy
              </button>
            </RedDot>

            <span style={{textAlign: "center"}}>Let the magic begin!</span>

          </center> 
        </div>

        <Modal
          open={this.state.seedPhraseModal}
          onClose={this.onSeedPhraseModalClose}
          center
          styles={{
            overlay: {
              backgroundImage: "https://discourse-cdn-sjc2.com/standard10/uploads/grin/original/1X/b7682ee4f4929f4e74a3f563af2f2e5ae77c8ed0.png"
            },
            modal: {
              // Red Dot styles b/c className doesn't work
              backgroundColor: "red",
              textAlign: "center",
              margin: "auto",
              display: "table-cell",
              verticalAlign: "middle",
              borderRadius: "50%",
              height: "100vh",
              width: "100vh",
              justifyContent: "center"
            },
        }}
        >
          <center>
            <h3> Yay you can have the receive grin!!</h3>
            <input defaultValue={this.state.seedPhrase || "Insert seed phrase here"} />
            <h3> DO NOT LOSE THIS SUPER SECRET KEY!!</h3>
            <div className="intention">
              <input
                type="checkbox"
                onChange={() => this.handleIntentions("intention-safe")}
              />
              I do solemnly swear that I will keep my Secret Spell safe
            </div>
            <div className="intention">
              <input
                type="checkbox"
                value="intention-share"
                onChange={() => this.handleIntentions("intention-share")}/>
              I do solemnly swear that I will not share my Secret Spell
            </div>
            <RedDot radius="10vw"> 
              <button className="red-dot-button" onClick={this.checkIntentions} >
                Receive Secret Spell
              </button>
            </RedDot>
          </center>
        </Modal>

      </div>
    );
  }
}