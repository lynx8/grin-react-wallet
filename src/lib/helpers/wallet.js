
import axios from 'axios';

const output_url = 'http://localhost:13420/v1/wallet/owner/retrieve_outputs';
const txs_url = 'http://localhost:13420/v1/wallet/owner/retrieve_txs';
const wallet_info_url = 'http://localhost:13420/v1/wallet/owner/retrieve_summary_info';
const node_height_url = 'http://localhost:13420/v1/wallet/owner/node_height';
const send_url = 'http://localhost:13420/v1/wallet/owner/issue_send_tx';



export const showSender = () => {
  // showSenderEmitter.emit(true);
}

/**
 * Refresh the height from the wallet's preferred node
 * Should be run fairly frequently to let user know when
 * to update
 */

export const refreshHeight = () => {
  axios.get(node_height_url)
    .then(heightInfo => {
        // totalFailureEmitter.emit(false);
        if (heightInfo[1]) {
         const currentNodeHeight = heightInfo[0];
         return currentNodeHeight
        }
      })
    .catch(error => {
      // totalFailureEmitter.emit(true);
    });
}

/**
 * Refresh all wallet info against the preferred node
 * Can potentially take a while, so user should be blocked
 * while this is happening
 */

export const refreshWalletFromNode = () => {
  // isUpdatingEmitter.emit(true);
  // just do a wallet info with with a refresh, ignore the result
  const wallet_refresh_url = wallet_info_url + '?refresh';
  return axios
    .get(wallet_refresh_url)
    .then(walletInfo => { 
      console.log('refresh wallet', walletInfo);
      return (
      (walletInfo && (walletInfo || {}).data) 
        ? walletInfo.data[1]
        : null)
      })
}

/**
 * Rest-y type functions
 */

export const getOutputs = (tx_id, show_spent) => {
  let url = output_url;
  if (tx_id != null) {
    url += '?tx_id=' + tx_id;
  }
  if (show_spent === true) {
    if (tx_id != null) {
      url += '&show_spent=true'; // are these for state handling in Angular or part of the Grin API?
    } else {
      url += '?show_spent=true';
    }
  }
  console.log('Calling URL ' + url);
  return axios.get(url)
    .then(data => {
      console.log('get outputs data', data);
      data.map(outputs_response => {
      return outputs_response[1];
    })});
}

export const getTxLog = (id) => {
  return axios.get(txs_url + '?id=' + id)
    .then(data => {
      data.map(tx_response => {
      return tx_response[1][0];
    })});
}

export const getTxLogs = () => {
  return axios.get(txs_url)
    .then(res => {
      console.log('get tx logs', res);
      return res.data ? res.data.map(tx_response => {
        return tx_response[1];
      })
      : null;
  });
}

export const getWalletInfo = () => {
  return axios.get(wallet_info_url)
    .then(res => {
      res.data.map(wallet_response => {
      return wallet_response[1];
    })});
}


export const postSend = (args) => {
  console.log('Posting - ' + send_url);
  console.dir(args);
  axios.post(send_url, args)
    .then((res) => {
      console.log('wallet send suc', res);
    })
    .catch((error) => {
      console.log('wallet send err', error);
    });
}

/**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
export const handleError = (operation = 'operation', result) => {
  return (error) => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return result;
  };
}

/** Log a message with the MessageService */
export const log = (message) => {
  console.log('WalletService: ' + message);
}
