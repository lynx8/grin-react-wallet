import React, {Component} from 'react';
import ListTransactions from './ListTransactions';

/**
 * @name - 
 * @summary - 
 * @description - 
 * @prop - 
 */
export default class AccountInfo extends Component {
  constructor(props){
    super(props);
    this.state = {
      isHidden: true,
      isDetailed: false
    }
  }
  
  revealAccountInfo = () => {
    console.log('reveal');
    this.setState({isHidden: false});
  };

  hideAccountinfo = () => {
    this.setState({isHidden: true});
  };

  renderAccountDetails = () => {

    const {wallet} = this.props;
    console.log('account wallet', wallet);
    if(wallet) {
      const {
        amount_awaiting_confirmation,
        amount_currently_spendable,
        amount_immature,
        amount_locked,
        last_confirmed_height
      } = wallet
      
      const totalGrin =
        amount_awaiting_confirmation + 
        amount_currently_spendable + 
        amount_immature;

      const basicInfo = () => (
        <div id="account-info-basic"> 
          <p> Basic account info: </p>
          <p> Total Balance: {totalGrin}</p>
        </div>
      );

      const detailedInfo = () => (
        <div>
          Detailed account info
        </div>
      );

      const {isHidden, isDetailed} = this.state;

      return (
        <div className={isHidden ? 'hidden' : ''}>
          {basicInfo()}
          {isDetailed ? detailedInfo() : null}
        </div>
      )
    } else {
      return (
        <div>
          <p> Error: No wallet connected </p>
        </div>
      )
    }
  };

  render() {
    return (
      <div id="account-info" onClick={this.revealAccountInfo}>
        {this.renderAccountDetails()}
      </div>
    )
  }
}