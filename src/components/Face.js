import React, {Component} from 'react';
import {refreshWalletFromNode, getWalletInfo} from '../lib/helpers/wallet';

import AccountInfo from './AccountInfo';


/**
 * @name - 
 * @summary - 
 * @description - 
 * @prop - 
 */
export default class  extends Component {
/**
 * @name constructor
 * @summary - 
 * @description - 
 */
  constructor(props) {
    super(props);
    this.state = {
      isPrivateMode: true,
      txs: null,
      wallet: {},
      activeTx: null,
      chainData: null
    }
  }

  componentWillMount() {
    this.refreshWallet()
  }
  
  refreshWallet = () => {
    const walletInfo = getWalletInfo();
    console.log('ale', walletInfo);
    const wallet =  refreshWalletFromNode()
      .then((wallet) => {
        console.log('wallet init', wallet);
        if(
          wallet &&
          (wallet.last_confirmed_height
          !==
          this.state.wallet.last_confirmed_height)
        ){ // if no wallet or wallet is up to date
          console.log('update wallet');
          this.setState({wallet: wallet});
        }
      })
      .catch((error) => {
        console.log('wallet load error', error);
        return {error}
      })
  }

  render() {
    const {
      wallet
    } = this.state;

    return (
      <div>
        <h1> SCHMINII GRINII Account #<span role="img"> ⛑㊙️🍹👾 </span></h1> 
        <AccountInfo wallet={wallet}/>
      </div>
    );
  }
}