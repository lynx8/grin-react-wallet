import React, {Component} from 'react';
import {getTxLogs, getOutputs} from '../lib/helpers/wallet';

/**
 * @name - 
 * @summary - 
 * @description - 
 * @prop -
 *  Number: tx_id
 *  
 * 
 */
export default class  extends Component {
/**
 * @name constructor
 * @summary - 
 * @description - 
 */
  constructor(props) {
    super(props);
    this.state = {
      txs: null,
      outputs: null,
      inputs: null
    }
  }

  componentWillMount() {
    console.log('route_tx_id' + this.props.tx_id);
    this.renderTx();
  }

  getOutputs = async (id, show_spent) => {
    const outputs = await getOutputs(id, show_spent);
    console.log('Output-Listing subscription', outputs);
    if(outputs) {
      this.setState({outputs})
      return outputs
    } else {
      return null;
    }
  }

  renderOutputs = () => {
   return this.state.outputs ? 
      this.state.outputs.map((utxo) => {
        console.log('utxo', utxo);
        return (
          <div>
            <p> {utxo.tx_id} </p>
          </div>
        )
      })
      : null;
  }
  
  renderTx = async () => {
    const txLogs = await getTxLogs();
    console.log('logs', txLogs);
  }


  render() {
    return (
      <React.Fragment>
        {this.renderOutputs()}
      </React.Fragment>

    );
  }
}
