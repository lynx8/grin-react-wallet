import React from 'react';

/**
 * @name - 
 * @summary - 
 * @description - 
 * @prop - 
 */
export default (props) => {
  const {
    height,
    width,
    radius,
    customStyle,
    children
  } = props;

  const getStyle = () => {
    // TODO make circle responsive even out if height/width can not be matched 
    const baseStyle = {
      backgroundColor: "red",
      textAlign: "center",
      margin: "auto",
      display: "table-cell",
      verticalAlign: "middle",
      borderRadius: "50%",
      height: "10vh",
      width: "10vh",
      // boxShadow: "0.5vh 0.5vh 2vh gray",
      ...customStyle,
    }

    if(radius) {
      const radiusInt = /\d*/.exec(radius)[0];
      return {
        ...baseStyle,
        height: radius,
        width: radius,
        // boxShadow: `${radiusInt / 20}vh ${radiusInt / 20}vh ${radiusInt / 2}vh black`
      }
    }

    if(height && width) {
      return {
        ...baseStyle,
        height,
        width
      };
    }

    return baseStyle;
  }

  const style = getStyle();

  return (
    <div
      id={props.id}
      className={props.className}
      style={style}
    >
      {children}
    </div>
  )
}