import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Face from './components/Face';
import LoginPage from './screens/Login';
import SplashPage from './screens/Splash';

export default () => (
  <Router>
    <div>
      <nav id="nav-bar">
        <li> <Link to="/"> Home</Link> </li>
        <li> <Link to="/wallet/">Accounts</Link> </li>
        <li> <Link to="/login/">Login</Link> </li> 
      </nav>

      <Route path="/" exact component={SplashPage} />
      <Route path="/wallet/" component={Face} />
      <Route path="/login/" component={LoginPage} />
    </div>
  </Router>
);
